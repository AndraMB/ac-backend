const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");
//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
// app.get("/", (req, res) => {
//   res.status(200).send("works?")
// });

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end"
});
connection.connect(function(err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS cinema_clients(nume VARCHAR(255),prenume VARCHAR(255),telefon VARCHAR(255),email VARCHAR(255),numeFilm VARCHAR(255),data VARCHAR(255),ora INTEGER,nrBilete INTEGER,rand INTEGER,loc INTEGER)";
  connection.query(sql, function(err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let nume = req.body.nume;
  let prenume = req.body.prenume;
  let telefon = req.body.telefon;
  let email = req.body.email;
  let numeFilm = req.body.numeFilm;
  let data = req.body.data;
  let ora = req.body.ora;
  let nrBilete = req.body.nrBilete;
  let rand = req.body.rand;
  let loc = req.body.loc;
  let error = [];

  if (!nume||!prenume||!telefon||!email||numeFilm||!data||!ora||!nrBilete||!rand||!loc) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (nume.length < 2 || nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (prenume.length > 2 || nume.length < 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (prenume.mtch("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!email.includes("@gmail.com") || !email.includes("@yahoo.com")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }

    if (parseInt(rand) == "NaN") {
      console.log("Datele despre cinema(rand) invalide!");
      error.push("Datele despre cinema(rand) invalide!");
    } else if (parseInt(loc).toString() == "NaN") {
      console.log("Datele despre cinema(loc) invalide!");
      error.push("Datle despre cinema(loc) invalide!");
    } else if (parseInt(nrBilete) == "NaN") {
      console.log("Datele despre cinema(nrBilete) invalide!");
      error.push("Datele despre cinema(nrBilete) invalide!");
    }
  }
  if (errors.length === 0) {
    const sql =
      "INSERT INTO cinema_clients (nume,prenume,telefon,email,numeFilm,data,ora,nrBilete,rand,loc) VALUES ('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +numeFilm +"','" + data +"','" + ora +"','" +nrBilete +"','" +rand +"','" +loc +"')";
    connection.query(sql, function(err, result) {
      if (err) throw err;
      console.log("Rezervare adaugata in baza de date!");
      res.status(200).send({
        message: "Rezervare adaugata in baza de date!"
      });
      console.log(sql);
    });
  } else {
    res.status(500).send(error);
    console.log("Eroare la inserarea in baza de date!");
  }
});
